Instrucciones de uso:
Valla a la direccion de los archivos en el terminal. (cd <direccion de los archivos>)
Compile los archivo haciendo make all.
Procure que en la carpeta de los archivos se encuentre el documento fileserver.conf 
en el que la primera linea es el puerto y la segunda es la direccion donde estan los archivos con los que va a trabajar.
Para ejecutar el servidor ponga ./servidor
Ahora puede ejecutar el cliente poniendo ./cliente <host del servidor> <puerto del servidor>
Ingrese el comando USER para iniciar sesion usando el formato del enunciado.
Los comandos GET PUT SHARE LS RM siguen el formato del enunciado.
El comando PUT tiene una particularidad, solo se puede escibir en el archivo a crear una linea de texto.
�Disfrute del programa!

Cualquier duda, me pueden contactar a gacuchac@uc.cl y el c�digo est� a libre dispocici�n del que quiera.