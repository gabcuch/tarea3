#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>


int main()
{
        int sock, connected, bytes_recieved , true = 1;  
        char send_data [1024] , recv_data[1024];       

        struct sockaddr_in server_addr,client_addr;    
        int sin_size,n;
        
	//Para leer fileserver.conf
        FILE *fr;
        char puertochar[1000];
        int puertoint;
        char direccion[1000];

	//Para recibir mensaje del client
	char recibido[4][500];
	int contadormensaje = -1;
	char comparar[5];

	//Para comandos
	char comando[500];
	char nombre[500];
	char caracter[500];
	char data_temporal[1024];
	char texto[1024];
	int largotexto;

	//Leemos la primera linea del archivo fileserver.conf
        fr = fopen("fileserver.conf","r");
        fgets(puertochar, 1000, fr);
      
        //Guardamos el puerto
        puertoint = atoi(puertochar);
        //printf("Puerto: %i\n",puertoint);

        //Leemos la siguiente linea y guardamos el path
        fgets(direccion, 1000, fr);
	
/**	//Cambiamos al path
	strcpy(comando,"cd ");
	strcat(comando, direccion);
	fr = popen(comando,"r");
	if (fr == NULL) {
		printf("Failed to run command\n" );
               	////exit(1);    
       	}*/		
	pclose(fr);






	//Creamos el socket y sus detalles
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            perror("Socket");
            ////exit(1);
        }

        if (setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int)) == -1) {
            perror("Setsockopt");
            ////exit(1);
        }
        
        server_addr.sin_family = AF_INET;         
        server_addr.sin_port = htons(puertoint);     
        server_addr.sin_addr.s_addr = INADDR_ANY; 
        bzero(&(server_addr.sin_zero),8); 

        if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr))
                                                                       == -1) {
            perror("Unable to bind");
            ////exit(1);
        }

        if (listen(sock, 5) == -1) {
            perror("Listen");
            ////exit(1);
        }
		
	printf("\nTCPServer Waiting for client on port %d\n",puertoint);
        fflush(stdout);

	//Para no perder la conexion si el client se desconecta
        while(1)
        {  

            sin_size = sizeof(struct sockaddr_in);

            connected = accept(sock, (struct sockaddr *)&client_addr,&sin_size);

            /**printf("\n I got a connection from (%s , %d)",
                   inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));*/
	
	   //Limpiamos todo lo usado
			
	//Para recibir mensaje del client
	bzero(recibido[0],500);
	bzero(recibido[1],500);
	bzero(recibido[2],500);
	bzero(recibido[3],500);
	contadormensaje = -1;
	bzero(comparar,4);

	//Para comandos
	bzero(comando,500);
	bzero(nombre,500);
	bzero(caracter,500);
	bzero(data_temporal,1024);

	//Data
	bzero(send_data,1024);
	bzero(recv_data,1024);
		
            while (1)
            {
		contadormensaje++;
	      //le decimos al client que puede enviar
	      strcpy(send_data,"ENVIAR");
              send(connected, send_data,strlen(send_data), 0);  

              bytes_recieved = recv(connected,recv_data,1024,0);
	      strcpy(recibido[contadormensaje],recv_data);
              recv_data[bytes_recieved] = '\0';
	
	      //printf("Recibido: %s\n",recibido[contadormensaje]);

	      //Si client se desconecta
	      strcpy(comparar,recibido[contadormensaje]);
		
	      if(strcmp(comparar,"CLOSE\n") == 0)
		break;
	      //Si client termino de mandar
	      strncpy(comparar,recibido[contadormensaje],3);
	      comparar[3] = '\0';
	      printf("comparacion: %d comparar: %s\n",strcmp(comparar,"END"),comparar);


/////////////////////////////////////////////////////////////
	      if(strcmp(comparar,"END") == 0)
	      {
		printf("Comando: %s largo %zd contadormensaje %d\n",recibido[0],strlen(recibido[0]),contadormensaje);
		//Limpiamos el comando y el contadormensaje
		bzero(comando,500);
		contadormensaje = 0;
		
//////////////////////////////////////////////////////////////
		//Comparamos con el comando USER
		if(strcmp(recibido[0],"USER\n") == 0){
			printf("Comando: USER\n");
		
			strncpy(nombre, recibido[1]+6,strlen(recibido[1])-7);
			//printf("Nombre: %s",strnombre);
			strcpy(comando,"su ");
                	strcat(comando,nombre);
			//printf("Comando: %s",strcomando);
			FILE *fp;
	       		fp = popen( comando,"r");
			//popen( "password","r");
			//printf("Comando ejecutado\n");
  	       	 	if (fp == NULL) {
	        		printf("Failed to run command\n" );
	                	////exit(1);    
  	        	}
	        	else {
			
				strcpy(send_data, "User identified as ");
				strcpy(send_data, nombre);
				printf("%s\n",send_data);
			}	

			//Limpiamos todo lo usado
			
			//Para recibir mensaje del client
			bzero(recibido[0],500);
			bzero(recibido[1],500);
			bzero(recibido[2],500);
			bzero(recibido[3],500);
			bzero(recibido[4],500);
			contadormensaje = -1;
			bzero(comparar,4);

			//Para comandos
			bzero(comando,500);
			bzero(nombre,500);
			bzero(caracter,500);
			bzero(data_temporal,1024);
		
			//Data
			bzero(send_data,1024);
			bzero(recv_data,1024);
	
		}//Fin del if USER


//////////////////////////////////////////////////////////////
      		//Comparamos con el comando RM
		if(strcmp(recibido[0],"RM\n") == 0){
		      //printf("Entrando a RM");
		      FILE *fp;
		      strcpy(comando,"rm -f ");
		      bzero(nombre,500);
		      strncat(nombre,direccion,strlen(direccion)-1);
		      strcat(nombre,"/");
		      strncat(nombre, recibido[1]+6,strlen(recibido[1])-7); printf("nombre: %s\n",nombre);

		      strcat(comando,nombre);
		      //printf("comando: %s\n",comando);
		      fp = popen( comando,"r");
		      if (fp == NULL){
		             printf("Failed to run command\n");
	                     ////exit(1);
		      }//Fin del if fp == null
		      pclose(fp);
		      //printf("Archivo: %s eliminado\n",nombre);
		

		      //Avisamos a client que se elimino el archivo
		      bzero(send_data,1024);
		      strcpy(send_data,"OK\n");
		      strcat(send_data,"Message: File ");
		      strcat(send_data, nombre);
		      strcat(send_data," deleted\nEND\n");
		
 		      send(connected, send_data,strlen(send_data), 0);  

			//Limpiamos todo lo usado
			
			//Para recibir mensaje del client
			bzero(recibido[0],500);
			bzero(recibido[1],500);
			bzero(recibido[2],500);
			bzero(recibido[3],500);
			bzero(recibido[4],500);
			contadormensaje = -1;
			bzero(comparar,4);

			//Para comandos
			bzero(comando,500);
			bzero(nombre,500);
			bzero(caracter,500);
			bzero(data_temporal,1024);
		
			//Data
			bzero(send_data,1024);
			bzero(recv_data,1024);

	         }//Fin del if RM


///////////////////////////////////////////////////////////////
		//Comparamos con GET
		if(strcmp(recibido[0],"GET\n") == 0){
			printf("Entrando a GET\n");
			bzero(nombre,500);
			strncat(nombre,direccion,strlen(direccion)-1);
			strcat(nombre,"/");
			strncat(nombre, recibido[1]+6,strlen(recibido[1])-7); printf("nombre: %s\n",nombre);

			//Abriendo el archivo
			FILE *fp;
			fp = fopen(nombre,"r");
			if (fp == NULL){
			   printf("Failed to run command\n");
	                   ////exit(1);
		          }
			
			//Leyendo el archivo
			bzero(send_data,1024);
			while (1){
			if(fgets(caracter,60,fp) != NULL){
				//printf("%s",caracter);
				strcat(send_data,caracter); 
			}

			if (feof(fp)){
				printf("send_data: %s",send_data);
	  			strcat(data_temporal,"OK\nMessage: ");
				strcat(data_temporal,send_data);
	   			strcat(data_temporal,"\nEND\n");
				strcpy(send_data,data_temporal);
			break;}//Fin del if feof
			}//Fin del while 1 de lectura
			//Enviamos el mensaje al client
			printf("send_data: %s",send_data);
			send(connected, send_data,strlen(send_data), 0);  

			//Limpiamos todo lo usado
			
			//Para recibir mensaje del client
			bzero(recibido[0],500);
			bzero(recibido[1],500);
			bzero(recibido[2],500);
			bzero(recibido[3],500);
			bzero(recibido[4],500);
			contadormensaje = -1;
			bzero(comparar,4);

			//Para comandos
			bzero(comando,500);
			bzero(nombre,500);
			bzero(caracter,500);
			bzero(data_temporal,1024);

			//Data
			bzero(send_data,1024);
			bzero(recv_data,1024);

		}//Fin del if GET
///////////////////////////////////////////////////////////////
		//Comparamos con LS
		if(strcmp(recibido[0],"LS\n") == 0){
			char path[1035];
			strcpy(comando, "/bin/ls -lh ");
                	strcat(comando,direccion);
			FILE *fp;
	       		fp = popen( comando,"r");
  	        	if (fp == NULL) {
	        		printf("Failed to run command\n" );
	             	   ////exit(1);
  	        	}
	      	  	
			bzero(send_data,1024);
	  		while (fgets(path, sizeof(path)-1, fp) != NULL)
     		    	strcat(send_data, path);
	    
			pclose(fp);

			strcat(data_temporal,"OK\nMessage: ");
			strcat(data_temporal,send_data);
	   		strcat(data_temporal,"\nEND\n");
			strcpy(send_data,data_temporal);
			printf("send_data: %s",send_data);
			send(connected, send_data,strlen(send_data), 0);  
			//Limpiamos todo lo usado
			
			//Para recibir mensaje del client
			bzero(recibido[0],500);
			bzero(recibido[1],500);
			bzero(recibido[2],500);
			bzero(recibido[3],500);
			bzero(recibido[4],500);
			contadormensaje = -1;
			bzero(comparar,4);

			//Para comandos
			bzero(comando,500);
			bzero(nombre,500);
			bzero(caracter,500);
			bzero(data_temporal,1024);

			//Data
			bzero(send_data,1024);
			bzero(recv_data,1024);

		}//Fin del if LS

//////////////////////////////////////////////////////////////
		//Comparamos con SHARE
		if(strcmp(recibido[0],"SHARE\n") == 0){
			printf("Entrando a SHARE\n");
			bzero(nombre,500);
			bzero(comando,500);
			strncat(nombre,direccion,strlen(direccion)-1);
			strcat(nombre,"/");
			strncat(nombre, recibido[1]+6,strlen(recibido[1])-7); printf("nombre: %s\n",nombre);

			strcpy(comando,"chmod ugo+rw ");
			strcat(comando,nombre);
			//Abriendo el archivo
			FILE *fp;
			fp = popen(comando,"r");
			if (fp == NULL){
			   printf("Failed to run command\n");
	                   ////exit(1);
		          }

			pclose(fp);

			bzero(send_data,1024);
			bzero(data_temporal,500);
			strcpy(send_data, nombre);
			strcat(data_temporal,"OK\nMessage: File ");
			strcat(data_temporal,send_data);
	   		strcat(data_temporal," shared\nEND\n");
			strcpy(send_data,data_temporal);
			printf("send_data: %s",send_data);
			send(connected, send_data,strlen(send_data), 0);  
			//Limpiamos todo lo usado
			
			//Para recibir mensaje del client
			bzero(recibido[0],500);
			bzero(recibido[1],500);
			bzero(recibido[2],500);
			bzero(recibido[3],500);
			bzero(recibido[4],500);
			contadormensaje = -1;
			bzero(comparar,4);

			//Para comandos
			bzero(comando,500);
			bzero(nombre,500);
			bzero(caracter,500);
			bzero(data_temporal,1024);

			//Data
			bzero(send_data,1024);
			bzero(recv_data,1024);

			
		}//Fin del if SHARE

///////////////////////////////////////////////////////////////
		//Comparamos con PUT
		if(strcmp(recibido[0],"PUT\n") == 0){
			printf("Entrando a PUT\n");
			bzero(nombre,500);
			strncat(nombre,direccion,strlen(direccion)-1);
			strcat(nombre,"/");
			strncat(nombre, recibido[1]+6,strlen(recibido[1])-7); printf("nombre: %s\n",nombre);

			//Abriendo el archivo
			FILE *fp;
			fp = fopen(nombre,"w");
			if (fp == NULL){
			   printf("Failed to run command\n");
	                   //////exit(1);
		          }
			fprintf(fp,"%s",recibido[3]);
			fclose(fp);
			
			bzero(send_data,1024);
			bzero(data_temporal,500);
			//strcpy(send_data, nombre);
			strcat(data_temporal,"OK\nMessage: File ");
			strcat(data_temporal,nombre);
	   		strcat(data_temporal," saved\nEND\n");
			strcpy(send_data,data_temporal);
			printf("send_data: %s",send_data);
			send(connected, send_data,strlen(send_data), 0);  
			//Limpiamos todo lo usado
			
			//Para recibir mensaje del client
			bzero(recibido[0],500);
			bzero(recibido[1],500);
			bzero(recibido[2],500);
			bzero(recibido[3],500);
			bzero(recibido[4],500);
			contadormensaje = -1;
			bzero(comparar,4);

			//Para comandos
			bzero(comando,500);
			bzero(nombre,500);
			bzero(caracter,500);
			bzero(data_temporal,1024);

			//Data
			bzero(send_data,1024);
			bzero(recv_data,1024);

		}//Fin del if PUT
	      }//Fin del if END
	      
	      //contadormensaje++;
              fflush(stdout);
            }//Fin del while 1 recibir mensaje 
        }//Fin del while 1 recibir conexion       

      close(sock);
      return 0;
} 
