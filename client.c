#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>


int main(int argc, char *argv[])

{

        int sock, bytes_recieved, port_number;  
        char send_data[1024],recv_data[1024];
        struct hostent *host;
        struct sockaddr_in server_addr;  

        

    // Controlamos el error que no se hayan especificado los datos necesario (phost y puerto)
    char buffer[500];
    if (argc < 3) {
       fprintf(stderr,"Usage %s hostname port\n", argv[0]);
       exit(0);
    }

    // Extraemos el puerto
    port_number = atoi(argv[2]);



     if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            perror("Socket");
            exit(1);
     }

     host = gethostbyname(argv[1]);
     if (host == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
     }

        server_addr.sin_family = AF_INET;     
        server_addr.sin_port = htons(port_number);   
        server_addr.sin_addr = *((struct in_addr *)host->h_addr);
        bzero(&(server_addr.sin_zero),8); 

        if (connect(sock, (struct sockaddr *)&server_addr,
                    sizeof(struct sockaddr)) == -1) 
        {
            perror("Connect");
            exit(1);
        }

        while(1)
        {
        
          bytes_recieved=recv(sock,recv_data,1024,0);
          recv_data[bytes_recieved] = '\0';
 
          if (strcmp(recv_data , "q") == 0 || strcmp(recv_data , "CLOSE") == 0)
          {
           close(sock);
           break;
          }

          else if(strcmp(recv_data,"ENVIAR") !=0 && strcmp(recv_data,"ENVIAR\n") !=0 && strcmp(recv_data,"\nENVIAR") !=0 && strcmp(recv_data,"\nENVIAR\n") !=0)
           printf("\nRecieved data = %s " , recv_data);
           
           printf("\nSEND (CLOSE to quit) : ");
           fgets(send_data,255,stdin);
           
          if (strcmp(send_data , "CLOSE\n") != 0)
           send(sock,send_data,strlen(send_data), 0); 

          else
          {
           send(sock,send_data,strlen(send_data), 0);   
           close(sock);
           break;
          }
        
        }   
return 0;
}




